<table class="table table-hover">
    <a href="{{route('telefone.create')}}" class="label label-primary">novo telefone</a>
    @foreach ($telefones as $telefone)
        <tr>
            <td>{{ $telefone->descrição }}</td>
            <td>{{ $telefone->numero }}</td>
            <td>
                <a href="{{route('telefone.delete', ['id' => $telefone->id])}}" class="text-danger" data-toggle="tooltip" data-placement="top" title="Excluir"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    @endforeach
</table>
