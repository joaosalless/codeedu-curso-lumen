<div class="panel {{ $pessoa->sexo == 'F' ? 'panel-danger' : 'panel-info' }}">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="fa fa-{{ $pessoa->sexo == 'F' ? 'male' : 'female' }}"></i>
            {{ $pessoa->apelido }}
            <span class="pull-right">
                <a href="{{route('pessoa.edit', ['id' => $pessoa->id])}}" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-edit"></i></a>
                <a href="{{route('pessoa.delete', ['id' => $pessoa->id])}}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Excluir"><i class="fa fa-trash"></i></a>
            </span>
        </h3>
    </div>
    <div class="panel-body">
        <h3>{{ $pessoa->nome }}</h3>
        @include('partials.telefones', ['telefones' => $pessoa->telefones])
    </div>
</div>
