<?php

use CodeAgenda\Entities\Pessoa;
use CodeAgenda\Entities\Telefone;
use Illuminate\Database\Seeder;

class TelefoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Pessoa::all() as $pessoa) {
            factory(Telefone::class, 3)->create([
                'pessoa_id' => $pessoa->id
            ]);
        }
    }
}
