<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(CodeAgenda\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(CodeAgenda\Entities\Pessoa::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->name,
        'apelido' => $faker->firstname,
        'sexo' => $faker->randomElement(['M', 'F']),
    ];
});

$factory->define(CodeAgenda\Entities\Telefone::class, function (Faker\Generator $faker) {
    return [
        'descrição' => $faker->randomElement(['TRABALHO', 'CASA', 'CELULAR']),
        'cod_país' => rand(1, 199),
        'ddd' => rand(10, 99),
        'prefixo' => rand(2000, 9999),
        'sufixo' => rand(2000, 9999),
    ];
});
