<?php

namespace CodeAgenda\Http\Controllers;

use Illuminate\Http\Request;
use CodeAgenda\Entities\Pessoa;

class AgendaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index($letra = 'A')
    {
        $pessoas = Pessoa::where('apelido', 'like', $letra.'%')->get();

        return view('agenda', compact('pessoas'));
    }

    public function destroy($id)
    {
        Pessoa::destroy($id);

        return redirect()->route('agenda.index');
    }

    public function busca(Request $request)
    {
        $busca = $request->get('busca');

        if (!empty($busca)) {
            $pessoas = Pessoa::where('nome', 'like', "%{$busca}%")
                 ->orWhere('apelido', 'like', "%{$busca}%")
                 ->get();
        } else {
            $pessoas = Pessoa::all();
        }

        return view('agenda', compact('pessoas'));
    }
}
