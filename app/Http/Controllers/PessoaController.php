<?php

namespace CodeAgenda\Http\Controllers;

use Illuminate\Http\Request;
use CodeAgenda\Entities\Pessoa;
use Illuminate\Support\Facades\Validator;

class PessoaController extends Controller
{
    public function create()
    {
        $errors = [];

        return view('pessoa.create', compact('errors'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nome'    => 'required|unique:pessoas|min:3|max:255',
            'apelido' => 'required|min:2|max:50',
            'sexo'    => 'required',
        ]);

        $errors = $validator->messages();

        if ($validator->fails()) {
            return view('pessoa.create', compact('errors'));
        }

        $pessoa = Pessoa::create($request->all());
        $letra = strtoupper(substr($pessoa->apelido, 0, 1));

        return redirect()->route('agenda.letra', compact('letra'));
    }

    public function edit($id)
    {
        $pessoa = Pessoa::find($id);
        $errors = [];

        return view('pessoa.edit', compact('pessoa', 'errors'));
    }

    public function update(Request $request, $id)
    {
        $pessoa = Pessoa::find($id);

        $validator = Validator::make($request->all(), [
            'nome'    => 'required|min:3|max:255|unique:pessoas,nome,'.$pessoa->id,
            'apelido' => 'required|min:2|max:50',
            'sexo'    => 'required',
        ]);

        $errors = $validator->messages();

        if ($validator->fails()) {
            return view('pessoa.create', compact('errors'));
        }

        $pessoa->fill($request->all())->save();
        $letra = strtoupper(substr($pessoa->apelido, 0, 1));

        return redirect()->route('agenda.letra', compact('letra'));
    }

    public function delete($id)
    {
        $pessoa = Pessoa::find($id);

        return view('pessoa.delete', compact('pessoa'));
    }

    public function destroy($id)
    {
        Pessoa::destroy($id);

        return redirect()->route('agenda.index');
    }
}
